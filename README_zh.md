## MBassador

## 简介

MBassador 是一个发布订阅模式的三方库

- 支持事件发布，订阅
- 支持同步异步发送事件
- 支持事件过滤
- 支持设置接收事件的强弱引用
- 支持事件注解驱动

## 下载安装

```
ohpm install @ohos/mbassador
```
OpenHarmony ohpm 环境配置等更多内容，请参考如何安装 [OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 注解

| 注解 |  功能 |
|------|------|
| @Handler | 事件接收处理程序 |
| @Listener | 可用于自定义侦听器范围的配置，如使用的引用类型 |

## 属性描述说明

### @Handler

| Properties |  Description |
|------|------|
| filters?: IMessageFilter<Object>[] | 事件过滤 |
| delivery?: Invoke | 事件同步异步发布 |
| priority?: number | 事件优先级 |
| enabled?: boolean | 事件是否接收 |

### @Listener

| Properties |  Description |
|------|------|
| className| 事件接收对象name |
| References | 强弱引用对象设置 |

## 使用说明

### 监听器

```
import { Annotations, References } from '@ohos/mbassador'
......

@Annotations.Listener("ExampleListener",References.Weak)
export class ExampleListener {
  @Annotations.handle()
  firstMessage(str: string, listener: ExampleListener) {
    //do something
  }

  @Annotations.handle({priority:100000,enabled:false})
  secondMessage(str: string, listener: ExampleListener) {
    //do something
  }

  @Annotations.handle({filters:[new ExampleFilter],enabled:true})
  thirdMessage(str: string, listener: ExampleListener) {
    //do something
  }
}
```

### 同步

```
    let mbassador = new MBassador<String>()
    let listener = new ExampleListener(callback)
    mbassador.subscribe(listener);
    mbassador.post(new String("this is first")).now();
```

### 异步

```
    let mbassador:MBassador = new MBassador()
    let listener:ExampleListener = new ExampleListener(callback)
    mbassador.subscribe(listener);
    mbassador.post(new String("this is asynchronously message")).asynchronously();
```

### 解绑

```
   mbassador.unSubscribe(listener);
```

### 过滤器

```
import { IMessageFilter, SubscriptionContext } from '@ohos/mbassador'

export class ExampleFilter implements IMessageFilter<String> {
    accepts(msg: String, context: SubscriptionContext) {
          //some code
           return true;
    }
}
```
## 目录结构

```
|---- Mbassador
      |----mbassador 
           |----src
                |---- bus
                |---- config
                |---- dispatch
                |---- entry
                |---- interface
                |---- listener
                |---- subscription
                |---- utils
      |----entry
           |----src
                |---- main
                      |---- ets
                            |---- entryability
                                  |---- EntryAbility.ts
                            |---- pages
                                  |---- index.ets
```

## 约束与限制
在下述版本验证通过：

- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)
- DevEco Studio: 4.0 (4.0.3.512), SDK: API10 (4.0.10.9)
- DevEco Studio: 3.1 Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/ohos-mbassador/issues)，当然，也非常欢迎开发者给ohos_mbassador发 [PR](https://gitee.com/openharmony-sig/ohos-mbassador/pulls) 。


## 许可证
本项目基于 [MIT License](https://gitee.com/openharmony-sig/ohos_mbassador/blob/master/LICENSE) ，请自由地享受和参与开源。